﻿using Isopoh.Cryptography.Argon2;
using Isopoh.Cryptography.SecureArray;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repository.Entities;
using Repository.Entities.Enum;
using Service.DTO;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Service.Services
{
    public class AuthService : IAuthService
    {
        private readonly JwtSettings _jwtSettings;

        public AuthService(IOptions<JwtSettings> jwtSettings)
        {
            _jwtSettings = jwtSettings.Value;
        }

        public JwtSecurityToken GenerateToken(User user)
        {
            List<Claim> userClaim = GetClaims(user);

            List<Claim> claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            }
            .Union(userClaim)
            .ToList();

            if (_jwtSettings.Key == null)
                throw new Exception($"jwtSettings does not contain a Key value.");

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSettings.AccessTokenDurationInMinutes),
                signingCredentials: signingCredentials);

            return jwtSecurityToken;
        }

        private List<Claim> GetClaims(User user)
        {
            List<Claim> claims = new List<Claim>()
            {
                new Claim(nameof(user.Name), user.Name),
                new Claim(nameof(user.Email), user.Email),
            };

            List<Claim> roleClaims = new List<Claim>();

            IEnumerable<UserRole> roles = user.Roles;

            foreach (UserRole role in roles)
                roleClaims.Add(new Claim(nameof(Role), role.Role.ToString()));


            IEnumerable<Claim> finalClaims = claims.Union(roleClaims);

            return finalClaims.ToList();
        }

        public (string, byte[]) HashPassword(string password)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[16];
            RandomNumberGenerator.Create().GetBytes(salt);

            Argon2Config passwordWithConfig = new Argon2Config
            {
                Password = passwordBytes,
                Salt = salt
            };

            Argon2 argon2 = new Argon2(passwordWithConfig);
            string hashedPassword;
            using (SecureArray<byte> hashedPasswordBytes = argon2.Hash())
                hashedPassword = passwordWithConfig.EncodeString(hashedPasswordBytes.Buffer);

            return (hashedPassword, salt);
        }

        public bool VerifyPassword(string hashedPassword, string password, byte[] salt)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            Argon2Config passwordConfigToVerify = new Argon2Config
            {
                Password = passwordBytes,
                Salt = salt
            };

            SecureArray<byte>? hash = null;
            try
            {
                if (passwordConfigToVerify.DecodeString(hashedPassword, out hash) && hash is not null)
                {
                    var argon2ToVerify = new Argon2(passwordConfigToVerify);
                    using (var hashToVerify = argon2ToVerify.Hash())
                    {
                        return Argon2.FixedTimeEquals(hash, hashToVerify);
                    }
                }

                return false;
            }
            finally
            {
                hash?.Dispose();
            }
        }

        public string GenerateRefreshToken()
        {
            using (RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create())
            {
                byte[] randomBytes = new byte[_jwtSettings.RefreshTokenSize];
                randomNumberGenerator.GetBytes(randomBytes);

                return Convert.ToBase64String(randomBytes);
            }
        }
    }
}
