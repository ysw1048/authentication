﻿using Repository.Entities;
using System.IdentityModel.Tokens.Jwt;

namespace Service.Services
{
    public interface IAuthService
    {
        string GenerateRefreshToken();
        JwtSecurityToken GenerateToken(User user);
        (string, byte[]) HashPassword(string password);
        bool VerifyPassword(string hashedPassword, string password, byte[] salt);
    }
}