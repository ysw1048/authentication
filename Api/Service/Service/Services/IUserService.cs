﻿using Repository.Entities;
using Service.DTO.Requests;
using Service.DTO.Response;

namespace Service.Services
{
    public interface IUserService
    {
        Task<AddUserResponseDTO> AddUser(AddUserRequestDTO createUserRequestDTO);
        Task<User> GetUser(int userId);
        Task<LoginResponseDTO> login(LoginRequestDTO loginRequestDTO);
        Task<TokenResponseDTO> RefreshAccessToken(RefreshAccessTokenDto refreshAccessTokenDto);
        Task RemoveUser(User user);
        Task<User> UpdateUser(User user);
    }
}