﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repository.Entities;
using Repository.Entities.Enum;
using Repository.Repositories;
using Service.DTO;
using Service.DTO.Requests;
using Service.DTO.Requests.Validators;
using Service.DTO.Response;
using Service.Exceptions;
using System.IdentityModel.Tokens.Jwt;

namespace Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAuthService _authService;
        private readonly IAuthMethodRepository _authMethodRepository;
        private readonly JwtSettings _jwtSettings;

        public UserService(
            IUserRepository userRepository,
            IAuthService authService,
            IAuthMethodRepository authMethodRepository,
            IOptions<JwtSettings> jwtSettings)
        {
            _userRepository = userRepository;
            _authService = authService;
            _authMethodRepository = authMethodRepository;
            _jwtSettings = jwtSettings.Value;
        }

        public async Task<AddUserResponseDTO> AddUser(AddUserRequestDTO createUserRequestDTO)
        {
            var validationResult = new AddUserRequestDTOValidator().Validate(createUserRequestDTO);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult);

            var (hashedPassword, salt) = _authService.HashPassword(createUserRequestDTO.Password);
         
            User user = new User()
            {
                Email = createUserRequestDTO.Email,
                Name = createUserRequestDTO.Name,
                Roles = new List<UserRole> { new UserRole() { Role = Role.USER } }
            };

            JwtSecurityToken jwtSecurityToken = _authService.GenerateToken(user);

            AuthMethod authMethod = new AuthMethod()
            {
                MethodType = MethodType.Password,
                MethodValue = hashedPassword,
                Salt = salt,
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                RefreshToken = _authService.GenerateRefreshToken(),
                RefreshTokenExpireDate = DateTime.UtcNow.AddDays(_jwtSettings.RefreshTokenDurationInDays)
            };

            user.AuthMethods = new List<AuthMethod>() { authMethod };

            User addedUser = await _userRepository.Add(user);

            //AuthMethod addedAuthMethod = await _authMethodRepository.Add(authMethod);
            Token accessToken = new Token
            {
                TokenValue = authMethod.AccessToken,
                ExpireDate = jwtSecurityToken.ValidTo
            };

            Token refreshToken = new Token
            {
                TokenValue = authMethod.RefreshToken,
                ExpireDate = authMethod.RefreshTokenExpireDate
            };

            await _authMethodRepository.Update(authMethod);

            AddUserResponseDTO addUserResponseDTO = new AddUserResponseDTO()
            {
                Email = addedUser.Email,
                Name = addedUser.Name,
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            return addUserResponseDTO;
        }

        public async Task<User> GetUser(int userId) => await _userRepository.Get(userId);

        public async Task<User> UpdateUser(User user) => await _userRepository.Update(user);

        public async Task RemoveUser(User user) => await _userRepository.Remove(user);

        public async Task<LoginResponseDTO> login(LoginRequestDTO loginRequestDTO)
        {
            var validationResult = new LoginRequestDTOValidator().Validate(loginRequestDTO);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult);

            User user = await _userRepository.GetByEmail(loginRequestDTO.Email);

            AuthMethod? authMethod = user.AuthMethods
                .FirstOrDefault(auth => auth.UserId == user.Id && auth.MethodType == MethodType.Password);

            if (authMethod is null)
                throw new AuthMethodNotFoundException($"The user {user.Email} has a different authentication method");

            string hashedPassword = authMethod.MethodValue;
            byte[] salt = authMethod.Salt;

            bool result = _authService.VerifyPassword(hashedPassword, loginRequestDTO.Password, salt);

            if (!result)
                throw new Exception($"User {user.Email} does not match with given data.");

            JwtSecurityToken jwtSecurityToken = _authService.GenerateToken(user);

            authMethod.AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            Token accessToken = new Token
            {
                TokenValue = authMethod.AccessToken,
                ExpireDate = jwtSecurityToken.ValidTo,
            };

            authMethod.RefreshToken = _authService.GenerateRefreshToken();
            authMethod.RefreshTokenExpireDate = DateTime.UtcNow.AddDays(_jwtSettings.RefreshTokenDurationInDays);

            await _authMethodRepository.Update(authMethod);

            Token refreshToken = new Token
            {
                TokenValue = authMethod.RefreshToken,
                ExpireDate = authMethod.RefreshTokenExpireDate,
            };

            LoginResponseDTO response = new LoginResponseDTO
            {
                Email = user.Email,
                Name = user.Name,
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            return response;
        }

        public async Task<TokenResponseDTO> RefreshAccessToken(RefreshAccessTokenDto refreshAccessTokenDto)
        {
            var validationResult = new RefreshAccessTokenDTOValidator().Validate(refreshAccessTokenDto);

            if (!validationResult.IsValid)
                throw new ValidationException(validationResult);

            User user = await _userRepository.GetByEmail(refreshAccessTokenDto.Email);

            AuthMethod? authMethod = user.AuthMethods
                .FirstOrDefault(auth => auth.UserId == user.Id && auth.MethodType == MethodType.Password);

            if (authMethod is null)
                throw new AuthMethodNotFoundException($"The user {user.Email} has a different authentication method");

            if (authMethod.AccessToken != refreshAccessTokenDto.AccessToken
                || authMethod.RefreshToken != refreshAccessTokenDto.RefreshToken
                || authMethod.RefreshTokenExpireDate < DateTime.UtcNow)
                throw new UnauthorizedException();

            JwtSecurityToken jwtSecurityToken = _authService.GenerateToken(user);

            Token accessToken = new Token
            {
                TokenValue = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                ExpireDate = jwtSecurityToken.ValidTo,
            };

            authMethod.RefreshToken = _authService.GenerateRefreshToken();
            authMethod.RefreshTokenExpireDate = DateTime.UtcNow.AddDays(_jwtSettings.RefreshTokenDurationInDays);

            await _authMethodRepository.Update(authMethod);

            Token refreshToken = new Token
            {
                TokenValue = authMethod.RefreshToken,
                ExpireDate = authMethod.RefreshTokenExpireDate,
            };

            TokenResponseDTO response = new TokenResponseDTO()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
            };

            return response;
        }
    }
}
