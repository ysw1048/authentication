﻿namespace Service.Exceptions
{
    public class AuthMethodNotFoundException : ApplicationException
    {
        public AuthMethodNotFoundException(string message) : base(message) { }
    }
}
