﻿namespace Service.DTO
{
    public class JwtSettings
    {
        public string Key { get; set; } = string.Empty;
        public string Issuer { get; set; } = string.Empty;
        public string Audience { get; set; } = string.Empty;
        public double AccessTokenDurationInMinutes { get; set; }
        public double RefreshTokenDurationInDays { get; set; }
        public string Uid { get; set; } = string.Empty;
        public int RefreshTokenSize { get; set; }
    }
}
