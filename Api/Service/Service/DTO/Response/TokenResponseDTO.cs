﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTO.Response
{
    public class TokenResponseDTO
    {
        public Token AccessToken { get; set; } = null!;
        public Token RefreshToken { get; set; } = null!;
    }
}
