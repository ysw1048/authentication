﻿namespace Service.DTO.Response
{
    public class LoginResponseDTO
    {
        public string Email { get; set; } = null!;
        public string Name { get; set; } = null!;
        public Token AccessToken { get; set; } = null!;
        public Token RefreshToken { get; set; } = null!;
    }
}
