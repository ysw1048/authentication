﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTO.Requests.Validators
{
    public class RefreshAccessTokenDTOValidator : AbstractValidator<RefreshAccessTokenDto>
    {
        public RefreshAccessTokenDTOValidator()
        {
            RuleFor(p => p.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress();

            RuleFor(p => p.AccessToken)
                .NotNull()
                .NotEmpty();

            RuleFor(p => p.RefreshToken)
                .NotNull()
                .NotEmpty();
        }
    }
}
