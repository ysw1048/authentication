﻿using FluentValidation;

namespace Service.DTO.Requests.Validators
{
    public class AddUserRequestDTOValidator : AbstractValidator<AddUserRequestDTO>
    {
        public AddUserRequestDTOValidator()
        {
            RuleFor(p => p.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress();

            RuleFor(p => p.Name)
                .NotNull()
                .NotEmpty();

            RuleFor(p => p.Password)
                .NotNull()
                .NotEmpty();
        }
    }
}
