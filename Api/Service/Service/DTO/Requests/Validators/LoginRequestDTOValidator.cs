﻿using FluentValidation;

namespace Service.DTO.Requests.Validators
{
    public class LoginRequestDTOValidator : AbstractValidator<LoginRequestDTO>
    {
        public LoginRequestDTOValidator()
        {
            RuleFor(p => p.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress();

            RuleFor(p => p.Password)
                .NotNull()
                .NotEmpty();
        }
    }
}
