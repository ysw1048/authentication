﻿namespace Service.DTO.Requests
{
    public class AddUserRequestDTO
    {
        public string Email { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
