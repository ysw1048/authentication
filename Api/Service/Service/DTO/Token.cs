﻿namespace Service.DTO
{
    public class Token
    {
        public string TokenValue { get; set; } = null!;
        public DateTime ExpireDate { get; set; }
    }
}
