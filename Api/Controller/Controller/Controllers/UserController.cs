﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.DTO.Requests;
using Service.DTO.Response;
using Service.Services;

namespace Controller.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<AddUserResponseDTO> AddUser([FromBody] AddUserRequestDTO createUserRequestDTO)
        {
            return await _userService.AddUser(createUserRequestDTO);
        }

        [HttpPost("login")]
        public async Task<LoginResponseDTO> Login([FromBody] LoginRequestDTO loginRequestDTO)
        {
            return await _userService.login(loginRequestDTO);
        }

        [HttpGet("Authenticate")]
        [Authorize]
        public StatusCodeResult Authenticate()
        {
            return Ok();
        }

        [HttpPost("RefreshToken")]
        public  async Task<TokenResponseDTO> RefreshToken([FromBody] RefreshAccessTokenDto refreshAccessTokenDto)
        {
            return await _userService.RefreshAccessToken(refreshAccessTokenDto);
        }
    }
}
