﻿using Microsoft.EntityFrameworkCore;
using Service.Exceptions;
using System.Net;
using System.Text.Json;

namespace Controller.Middlewares
{
    public class ExceptionMiddleware : IMiddleware
    {
        private readonly ILogger<ExceptionMiddleware> _logger;
        public ExceptionMiddleware(ILogger<ExceptionMiddleware> logger) => _logger = logger;

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await handleExceptionAsync(context, ex);
            }
        }

        private Task handleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;

            ErrorDetails errorDetails = new()
            {
                ErrorMessage = exception.Message
            };

            switch (exception)
            {
                case AlreadyExistsException alreadyExistsException:
                    statusCode = HttpStatusCode.Conflict;
                    break;
                case ValidationException validationException:
                    statusCode = HttpStatusCode.BadRequest;
                    errorDetails.ErrorMessage = string.Join(", ", validationException.Errors);
                    break;
                case AuthMethodNotFoundException:
                    statusCode = HttpStatusCode.NotFound;
                    break;
                case DbUpdateException dbUpdateException:
                    statusCode = HttpStatusCode.Conflict;
                    break;
                case UnauthorizedException:
                    statusCode = HttpStatusCode.Unauthorized;
                    break;
                default:
                    break;
            }

            errorDetails.ErrorStatusInt = (int)statusCode;
            errorDetails.ErrorStatus = statusCode;
            string problemJson = JsonSerializer.Serialize(errorDetails);

            context.Response.StatusCode = (int)statusCode;

            return context.Response.WriteAsync(problemJson);
        }

        private class ErrorDetails
        {
            public int ErrorStatusInt { get; set; }
            public HttpStatusCode ErrorStatus { get; set; }
            public string? ErrorMessage { get; set; }
        }
    }
}
