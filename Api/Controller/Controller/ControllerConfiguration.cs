﻿using Controller.Middlewares;

namespace Controller
{
    public static class ControllerConfiguration
    {
        public static IServiceCollection AddControllerDependency(this IServiceCollection services)
        {
            services.AddScoped<ExceptionMiddleware>();

            return services;
        }
    }
}
