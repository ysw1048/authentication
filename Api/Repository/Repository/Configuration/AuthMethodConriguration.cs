﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Repository.Entities;

namespace Repository.Configuration
{
    public class AuthMethodConriguration : IEntityTypeConfiguration<AuthMethod>
    {
        public void Configure(EntityTypeBuilder<AuthMethod> builder)
        {
            builder
                .ToTable(nameof(AuthMethod))
                .HasKey(e => e.Id);

            builder
                .HasIndex(e => new { e.UserId, e.MethodType })
                .IsUnique();
        }
    }
}
