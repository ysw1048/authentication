﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Repository.Entities;

namespace Repository.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .ToTable(nameof(User))
                .HasKey(e => e.Id);

            builder
                .HasIndex(e => e.Email)
                .IsUnique();

            builder
                .HasIndex(e => e.Name)
                .IsUnique();

            builder
                .HasMany(e => e.AuthMethods)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired();

            builder
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(nameof(UserRole.UserId))
                .IsRequired();
        }
    }
}
