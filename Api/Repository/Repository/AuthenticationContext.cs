﻿using Microsoft.EntityFrameworkCore;
using Repository.Configuration;
using Repository.Entities;

namespace Repository
{
    public class AuthenticationContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public AuthenticationContext(DbContextOptions<AuthenticationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new AuthMethodConriguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            //base.OnModelCreating(modelBuilder);
        }
    }
}
