﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository.Repositories;

namespace Repository
{
    public static class RepositoryConfiguration
    {
        public static IServiceCollection AddRepositoryDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AuthenticationContext>(options =>
                    options.UseNpgsql(configuration.GetConnectionString("ContextConnection")));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAuthMethodRepository, AuthMethodRepository>();

            return services;
        }
    }
}
