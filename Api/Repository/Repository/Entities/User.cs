﻿namespace Repository.Entities
{
    public class User : Entity
    {
        public string Email { get; set; } = null!;
        public string Name { get; set; } = null!;
        public IEnumerable<UserRole> Roles { get; set; } = null!;
        public IEnumerable<AuthMethod> AuthMethods { get; set; } = null!;
    }
}
