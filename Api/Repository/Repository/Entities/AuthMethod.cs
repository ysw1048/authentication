﻿using Repository.Entities.Enum;

namespace Repository.Entities
{
    public class AuthMethod : Entity
    {
        public int UserId { get; set; }
        public MethodType MethodType { get; set; }
        public string MethodValue { get; set; } = null!;
        public byte[] Salt { get; set; } = null!;
        public string AccessToken { get; set; } = null!;
        public string RefreshToken { get; set; } = null!;
        public DateTime RefreshTokenExpireDate { get; set; }
    }
}
