﻿using Repository.Entities.Enum;

namespace Repository.Entities
{
    public class UserRole : Entity
    {
        public int UserId { get; set; }
        public Role Role { get; set; }
    }
}
