﻿using Repository.Entities;

namespace Repository.Repositories
{
    public interface IAuthMethodRepository : IRepository<AuthMethod>
    {
    }
}
