﻿namespace Repository.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public Task<TEntity> Add(TEntity entity);
        public Task<TEntity> Get(int entityId);
        public Task<TEntity> Update(TEntity entity);
        public Task Remove(TEntity entity);
    }
}
