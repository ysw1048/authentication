﻿using Repository.Entities;

namespace Repository.Repositories
{
    public class AuthMethodRepository : Repository<AuthMethod>, IAuthMethodRepository
    {
        public AuthMethodRepository(AuthenticationContext authenticationContext) : base(authenticationContext)
        {
        }
    }
}
