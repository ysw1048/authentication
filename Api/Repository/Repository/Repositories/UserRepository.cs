﻿using Microsoft.EntityFrameworkCore;
using Repository.Entities;
using Repository.Exceptions;

namespace Repository.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(AuthenticationContext authenticationContext) : base(authenticationContext)
        {
        }

        public async Task<User> GetByEmail(string email)
        {
            try
            {
                User? user = await _authenticationContext.Users
                    .Include(e => e.AuthMethods)
                    .Include(e => e.Roles)
                    .FirstOrDefaultAsync(u => u.Email == email);

                if (user is null)
                    throw new EntityNotFoundException($"User with email: {email} does not exist");

                return user;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<User> GetByName(string name)
        {
            try
            {
                User? user = await _authenticationContext.Users.FirstOrDefaultAsync(u => u.Name == name);

                if (user is null)
                    throw new EntityNotFoundException($"User with name: {name} does not exist");

                return user;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
