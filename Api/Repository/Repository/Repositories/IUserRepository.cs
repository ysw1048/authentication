﻿using Repository.Entities;

namespace Repository.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByEmail(string email);
        Task<User> GetByName(string email);
    }
}