﻿using Microsoft.EntityFrameworkCore;
using Repository.Entities;
using Repository.Exceptions;

namespace Repository.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly AuthenticationContext _authenticationContext;

        public Repository(AuthenticationContext authenticationContext)
        {
            _authenticationContext = authenticationContext;
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            try
            {
                TEntity createdEntity = _authenticationContext.Set<TEntity>().Add(entity).Entity;
                await _authenticationContext.SaveChangesAsync();

                return createdEntity;
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException(((Npgsql.PostgresException)ex.InnerException!).MessageText);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> Get(int entityId)
        {
            try
            {
                TEntity? entity = await _authenticationContext.Set<TEntity>().FindAsync(entityId);

                if (entity is null)
                    throw new EntityNotFoundException();

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> Update(TEntity entityToUpdate)
        {
            try
            {
                TEntity updatedEntity;

                if (_authenticationContext.Set<TEntity>().Any(e => e.Id.Equals(entityToUpdate.Id)))
                    updatedEntity = _authenticationContext.Set<TEntity>().Update(entityToUpdate).Entity;
                else
                    updatedEntity = _authenticationContext.Set<TEntity>().Add(entityToUpdate).Entity;

                await _authenticationContext.SaveChangesAsync();

                return updatedEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Remove(TEntity entityToDelete)
        {
            try
            {
                _authenticationContext.Set<TEntity>().Remove(entityToDelete);
                await _authenticationContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
