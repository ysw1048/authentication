﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Repository.Migrations
{
    /// <inheritdoc />
    public partial class AddRefreshToken : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExpireDate",
                table: "AuthMethod",
                newName: "RefreshTokenExpireDate");

            migrationBuilder.AddColumn<string>(
                name: "AccessToken",
                table: "AuthMethod",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RefreshToken",
                table: "AuthMethod",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccessToken",
                table: "AuthMethod");

            migrationBuilder.DropColumn(
                name: "RefreshToken",
                table: "AuthMethod");

            migrationBuilder.RenameColumn(
                name: "RefreshTokenExpireDate",
                table: "AuthMethod",
                newName: "ExpireDate");
        }
    }
}
